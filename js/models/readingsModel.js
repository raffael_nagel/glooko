var readingsModel = function(view){
    this.view = view;
};

readingsModel.prototype = {

    listReadings: function(data){
        var self = this;
        var ReadingsPromise;
        ReadingsPromise = data.Readings.getAll();

        Promise.resolve(ReadingsPromise).then(function(values){
            self.view.listReadings(values);
            self.view.setDateRange();
        });
    },

    createReading: function(data){
        var self = this;

        var readingData = this.view.getNewReadingData();
        if(readingData == null)return;
        var createPromise = data.Readings.create(readingData);
        Promise.resolve(createPromise).then(function (status) {
            self.view.closeModal();

            ReadingsPromise = data.Readings.getAll();
            Promise.resolve(ReadingsPromise).then(function(values){
                readingsView.listReadings(values);
            });

        });
    }

};