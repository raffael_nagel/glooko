var readingsView = function(){
    this.$body = $("body");
    this.$content = $('.main-content');
};

readingsView.prototype = {

    listReadings: function(readings){

        if(readings.length < 1)return;

        for(var i = 0; i < readings.length; i++){
            var date = readings[i].timestamp;
            var year = date.substr(0,4);
            var month = date.substr(5,2);
            var day = date.substr(8,2);
            var time = date.substr(11,11);
            readings[i].date = month+"/"+day+"/"+year;
            readings[i].time = time;
            if(readings[i].meal){
                if(readings[i].meal == 'before_meal')readings[i].meal = "Before Meal";
                if(readings[i].meal == 'after_meal')readings[i].meal = "After Meal";
            }
        }

        var htmlTemplate;
        htmlTemplate = Templates.reading_list;
        var context = {
            "readings" : readings
        };

        var template = Handlebars.compile(htmlTemplate);
        var html = template(context);
        this.$content.empty();
        this.$content.append(html);
    },

    getNewReadingData: function(){
        var readingData = {};

        if($("#modal-reading-value").val() == '' || $("#modal-reading-date").val() == '' || $("#modal-reading-time").val() == ''){
            $('#sa-error').click();
            return null;
        }

        readingData.bg_value = $("#modal-reading-value").val();

        if($("#modal-reading-meal").val() == 'no_meal'){
            readingData.meal = null;
        }else{
            readingData.meal = $("#modal-reading-meal").val();
        }

        var date = $("#modal-reading-date").val().split("/");
        var time = $("#modal-reading-time").val();
        var timestamp = date[2] + "-" + date[0] + "-" + date[1] + "T" + time;


        readingData.timestamp = timestamp;
        return readingData;
    },

    closeModal: function(){
        $('.btn-close').click();
        $('#modal-reading-date').val("");
        $('#modal-reading-time').val("");
        $('#modal-reading-value').val("");
        $('#modal-reading-meal').val("no_meal");
    },

    isVisible: function(reading){
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(reading).offset().top;
        return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
    },

    setDateRange: function(){
        var self = this;
        var date_start = "", date_end = "";
        var min = 999;
        var max = 0;
        $('.reading-item').each(function() {
            if(self.isVisible($(this))){

                if(parseInt($(this).data('index')) < min){
                    date_start = $(this).data('date');
                    min = parseInt($(this).data('index'));
                }
                if(parseInt($(this).data('index')) > max){
                    var max_id = parseInt($(this).data('index')) - 1;
                    if ($('body').height() <= ($(window).height() + $(window).scrollTop())) {
                        max_id = parseInt($(this).data('index'));
                    }

                    date_end = $("#reading-"+max_id).data('date');
                    max = max_id;
                }
            }

        });
        $('.time-range').text(date_start + " - " + date_end);
    }

};