Templates = {};

Templates.reading_list = "" +
    "{{#each readings as |reading readingId|}}" +
    "<div class='col-md-12 col-sm-12 col-lg-12 reading-item' id='reading-{{readingId}}' data-index='{{readingId}}' data-date='{{reading.date}}'> " +
        "<div class='mini-stat clearfix bx-shadow bg-white'>" +
            "<span class='mini-stat-icon bg-danger'><i class='fa fa-tint'></i></span>" +
            "<div class='mini-stat-info text-right text-dark'>" +
                "<span class='counter text-dark timstamp'>"+

                    "{{reading.bg_value}}</span>" +
                    "{{#if reading.meal}}<i class='fa fa-spoon'></i>  {{  reading.meal  }}{{/if}} <i class='fa fa-calendar'></i>  {{  reading.date  }} <i class='fa fa-clock-o'></i> {{ reading.time}}           " +
            "</div>" +
        "</div>" +
    "</div>"
    + "{{/each}}";