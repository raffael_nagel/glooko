require.config({
    paths: {
        "jquery": "../assets/js/jquery.app"
    }
});

var readingsView = new readingsView();
var readingsModel = new readingsModel(readingsView);

define(['lib/data/Data','jquery'], function(data) {

    readingsModel.listReadings(data);


    $('.btn-save').click(function (ev) {
        readingsModel.createReading(data);
        return false;
    });

    $(window).scroll(function() {
        readingsView.setDateRange();
        if ($('body').height() <= ($(window).height() + $(window).scrollTop())) {
            console.log('bottom');
            $('#sa-toast').click();
        }
    });

});