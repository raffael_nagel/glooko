
!function($) {
    "use strict";

    var SweetAlert = function() {};

    SweetAlert.prototype.init = function() {

        //Toast
        $('#sa-toast').click(function(){
            swal({
                title: "No More Readings",
                text: "You reached the bottom!",
                timer: 1500,
                showConfirmButton: false
            });
        });

        $('#sa-error').click(function(){
            swal({
                title: "Invalid Data",
                text: "Reading not inserted!",
                timer: 1500,
                showConfirmButton: false
            });
        });


    },
        //init
        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing
    function($) {
        "use strict";
        $.SweetAlert.init()
    }(window.jQuery);